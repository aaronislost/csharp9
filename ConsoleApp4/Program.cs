﻿ 
SkipLocalsInit();

static unsafe void DemoZeroing()
{
    int i;
    System.Console.WriteLine(*&i);
    // Display 0 as the local variable is automatically initialized with the default value
}

[System.Runtime.CompilerServices.SkipLocalsInit]
static unsafe  void SkipLocalsInit()
{
    int i;
    System.Console.WriteLine(*&i); // Unpredictable output as i is not initialized
}
 