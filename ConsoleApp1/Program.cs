﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DoAction();
            [System.Diagnostics.Conditional("DEBUG")]
            static void DoAction()
            {
                Console.WriteLine("Performing action");
            }

            Console.WriteLine("Hello World!");

            Person p = new Person()
            {
                FirstName = "Aaron"
            };

            if (p is not null)
            {

            }
        }
    }

    public record PersonRecord(string FirstName, string LastName);
    

    public class Person
    {
        public string LastName { get; init; }
        public string FirstName { get; init; }
    }
}
