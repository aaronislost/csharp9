﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class InitOnlySetters
    {
        public class Person
        {
            public Person()
            {

            }
            public Person(string name)
            {
                this.Name = name;
            }

            //public void SetName(string name)
            //{
            //    this.Name = name;
            //}

            public string Name { get; init; }
        }


        [Fact]
        public void Test()
        {
            var person = new Person
            {
                Name = "Jane Doe" // Compile Error 
            };
        }
    }
}
