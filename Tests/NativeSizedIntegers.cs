﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class NativeSizedIntegers
    {
        [Fact]
        public void Test()
        {
            nint x = 3;
            int y = 3;
            long v = 10;

            nint.Equals(x, y); // False
            nint.Equals(x, (nint)y); // True

            var test1 = y + 1 > x; // True;
            var test2 = y - 1 == x; // False

            var test3 = typeof(nint); // System.IntPtr
            var test4 = typeof(nuint); // System.UIntPtr
            var test5 = (x + 1).GetType(); // System.IntPtr
            var test6 = (x + y).GetType(); // System.IntPtr
            var test7 = (x + v).GetType(); // System.Int64

            dynamic z = 1;
            var test8 = z + x; // RuntimeBinderException: '+' cannot be applied 'System.IntPtr' and 'System.IntPtr'
        }
    }
}
