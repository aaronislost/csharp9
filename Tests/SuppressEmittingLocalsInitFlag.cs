﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class SuppressEmittingLocalsInitFlag
    {
        [Fact]
        public void Test()
        {
        }
        static unsafe void DemoZeroing()
        {
            int i;
            Console.WriteLine(*&i);
            // Display 0 as the local variable is automatically initialized with the default value
        }
    }
}

//https://www.meziantou.net/csharp-9-improve-performance-using-skiplocalsinit.htm