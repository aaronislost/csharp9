//public abstract class Product
//{
//    protected string Name { get; }
//    protected int Id { get; }

//    protected Product(string name, int id)
//    {
//        Name = name;
//        Id = id;
//    }

//    public abstract ProductOrder Order(int quantity);
//}

//public class Book : Product
//{
//    public string ISBN { get; }

//    public Book(string name, int categoryId, string Isbn) : base(name, categoryId)
//    {
//        ISBN = Isbn;
//    }

//    public override BookOrder Order(int quantity) => new BookOrder { Quantity = quantity, Product = this };
//}

//public class Music : Product
//{
//    protected Format Format { get; }

//    public Music(string name, int categoryId, Format format) : base(name, categoryId)
//    {
//        Format = format;
//    }

//    public override MusicOrder Order(int quantity) => new MusicOrder { Quantity = quantity, Product = this };
//}

//public class ProductOrder
//{
//    public int Quantity { get; set; }
//}

//public class BookOrder : ProductOrder
//{
//    public Book Product { get; set; }
//}

//public class MusicOrder : ProductOrder
//{
//    public Music Product { get; set; }
//}

//public enum Format
//{
//    Mp3,
//    Disc
//}

//public class Program
//{
//    public static void Main(string[] args)
//    {
//        var book = new Book
//        {
//            Name = "My book",
//            Id = 1,
//            ISBN = "978-3-16-148410-0"
//        };

//        var music = new Music
//        {
//            Name = "My music",
//            Id = 2,
//            Format = Format.Disc
//        };

//        BookOrder orderBook = book.Order(1);
//        MusicOrder orderMusic = music.Order(1);
//    }
//}

//using System;

//namespace ConsoleApp2
//{
//    public abstract class Product
//    {
//        protected string Name { get; }
//        protected int Id { get; }

//        protected Product(string name, int id)
//        {
//            Name = name;
//            Id = id;
//        }

//        public abstract ProductOrder Order(int quantity);
//    }

//    public class Book : Product
//    {
//        public string ISBN { get; }

//        public Book(string name, int categoryId, string Isbn) : base(name, categoryId)
//        {
//            ISBN = Isbn;
//        }

//        public override BookOrder Order(int quantity) => new BookOrder { Quantity = quantity, Product = this };
//    }

//    public class Music : Product
//    {
//        protected Format Format { get; }

//        public Music(string name, int categoryId, Format format) : base(name, categoryId)
//        {
//            Format = format;
//        }

//        public override MusicOrder Order(int quantity) => new MusicOrder { Quantity = quantity, Product = this };
//    }

//    public class ProductOrder
//    {
//        public int Quantity { get; set; }
//    }

//    public class BookOrder : ProductOrder
//    {
//        public Book Product { get; set; }
//    }

//    public class MusicOrder : ProductOrder
//    {
//        public Music Product { get; set; }
//    }

//    public enum Format
//    {
//        Mp3,
//        Disc
//    }
//    public class Program
//    {
//        public static void Main(string[] args)
//        {
//            var book = new Book("My book", 1, "978-3-16-148410-0");


//            var music = new Music("My music",
//                 2,
//                Format.Disc
//            );

//            BookOrder orderBook = book.Order(1);
//            MusicOrder orderMusic = music.Order(1);
//        }
//    }
//}
////top level statements
////init only setters
////record types
////new pattern matching
////performance

////https://anthonygiretti.com/2020/08/19/introducing-c-9-native-sized-integers/


////http://zetcode.com/csharp/lambda-expression/