using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class Friend
    {
        public Friend() { }

        public Friend(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class TargetTypedNewExpressions
    {
        [Fact]
        public void Test()
        {
            Friend friend = new();
            Friend friend2 = new("Thomas", "Huber");

        }
      
    }
}

//https://www.thomasclaudiushuber.com/2020/09/08/c-9-0-target-typed-new-expressions/

