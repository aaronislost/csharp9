using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xunit;

namespace Tests
{
    public class AttributesOnLocalFunctions
    {
        [Fact]
        private void work()
        {
            DoAction();

            [Conditional("DEBUG")]
            static void DoAction()
            {
                Console.WriteLine("Performing action");
            }

        }
    }
}