using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class StaticAnonymousFunctions
    {
        const string text = "{0} is a beautiful country !"; // text must be declared as const

        void PromoteCountry(Func<string, string> func)
        {
            var countries = new List<string> { "Canada", "France", "Japan" };

            foreach (var country in countries)
                Console.WriteLine(func(country));
        }

        [Fact]
        public void Test()
        {
            PromoteCountry(static country => string.Format(text, country)); // text is not captured
        }
    }
}


//Microsoft details on that page all the rule that apply to this feature: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/proposals/csharp-9.0/static-anonymous-functions

//Among these here are the most important ones:

//A lambda or anonymous method may have a static modifier.The static modifier indicates that the lambda or anonymous method is a static anonymous function.
//A static anonymous function cannot capture state from the enclosing scope. As a result, locals, parameters, and this from the enclosing scope are not available within a static anonymous function.
//A static anonymous function cannot reference instance members from an implicit or explicit this or base reference.
//A static anonymous function may reference static members from the enclosing scope.
//A static anonymous function may reference constant definitions from the enclosing scope.