﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class FunctionPointers
    {
        [Fact]
        public void Test()
        {
        }
        unsafe class Example
        {
            void Conversions()
            {
                //delegate*<int, int, int> p1 = (a,b)=> { return; } ;
                //delegate* managed<int, int, int> p2 = ...;
                //delegate* unmanaged<int, int, int> p3 = ...;

                //p1 = p2; // okay p1 and p2 have compatible signatures
                //Console.WriteLine(p2 == p1); // True
                //p2 = p3; // error: calling conventions are incompatible
            }
        }
    }
}
