using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class TargetTypedConditionalExpressions
    {
         public class Product
        {
            private string _name;
            private int _categoryId;

            public Product(string name, int categoryId)
            {
                _name = name;
                _categoryId = categoryId;
            }
        }

        public class Book : Product
        {
            private string _ISBN;

            public Book(string name, int categoryId, string ISBN) : base(name, categoryId)
            {
                _ISBN = ISBN;
            }
        }

        public class Headset : Product
        {
            private bool _supportBluetooth;

            public Headset(string name, int categoryId, bool supportBluetooth) : base(name, categoryId)
            {
                _supportBluetooth = supportBluetooth;
            }
        }

        [Fact]
        public void Test()
        {
            Book aBook = new Book("VideoGame", 1, "000000000");
            Headset headset = new Headset("VideoGame", 1, true);
             Product anotherProduct = aBook ?? headset; // Compilation error prior C# 9, allowed by C# 9
        }
       
    }
}

